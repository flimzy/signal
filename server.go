package signal

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// StartFunc is used to start a service, such as an HTTP server.
type StartFunc func() error

// ShutdownFunc is used to (ideally, gracefully) shut down a service, such
// as an HTTP server. When the context is canceled, the function should
// abandon any attempt at a graceful shutdown, and return immediately.
type ShutdownFunc func(context.Context) error

// StartServer starts a server process by calling start. Whenever ctx is
// cancelled, shutdown is called. If shutdown returns an error, it is returned,
// otherwise start's error is returned.
func StartServer(ctx context.Context, start, shutdown func() error) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	errCh := make(chan error, 1)
	go func() {
		if err := start(); err != nil {
			errCh <- err
			cancel()
		}
		close(errCh)
	}()
	<-ctx.Done()
	if err := shutdown(); err != nil {
		return err
	}

	return <-errCh
}

// Start is a general-purpose wrapper around StartServer. It starts a process
// by calling start, then handles a graceful shutdown whenever a SIGINT or
// SIGTERM is received, or ctx is cancelled. On shutdown, Shutdown() is called
// with a timeout of t. If Shutdown() returns an error, that error is returned,
// otherwise Start()'s error is returned, unless that error is
// http.ErrServerClosed, in which case the graceful shutdown is considered
// successful, and nil is returned.
//
// If start is nil, it is ignored, and it is presumed that the service is
// already running. If stop is nil, it is also ignored, and this function will
// just block until a a signal is received, or the context is canceled.
func Start(ctx context.Context, start StartFunc, stop ShutdownFunc, t time.Duration) error {
	if start == nil {
		start = func() error { return nil }
	}
	if stop == nil {
		stop = func(_ context.Context) error { return nil }
	}
	ctx, cancel := signal.NotifyContext(ctx, os.Interrupt, syscall.SIGTERM)
	defer cancel()
	return StartServer(ctx,
		func() error {
			if err := start(); !errors.Is(err, http.ErrServerClosed) {
				return err
			}
			return nil
		},
		func() error {
			ctx, cancel := context.WithTimeout(context.Background(), t)
			defer cancel()
			return stop(ctx)
		},
	)
}
