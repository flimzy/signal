package examples

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"time"

	"gitlab.com/flimzy/signal"
)

func ExampleStart() {
	ts := httptest.NewServer(http.DefaultServeMux)
	url, _ := url.Parse(ts.URL)
	s := &http.Server{Addr: url.Host}
	ts.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()
	if err := signal.Start(ctx, s.ListenAndServe, s.Shutdown, 5*time.Second); err != nil {
		panic(err)
	}
	fmt.Println("Server gracefully exited")
	// Output: Server gracefully exited
}
