package signal

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sync"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"
)

func TestStartServer(t *testing.T) {
	type tt struct {
		ctx      context.Context
		start    func() error
		shutdown func() error
		err      string
	}
	tests := testy.NewTable()
	tests.Add("startup fails", tt{
		ctx:      context.Background(),
		start:    func() error { return errors.New("foo") },
		shutdown: func() error { return nil },
		err:      "foo",
	})
	tests.Add("success", func() interface{} {
		ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
		defer cancel()
		s := httptest.NewServer(http.HandlerFunc(http.NotFound))
		url, _ := url.Parse(s.URL)
		srv := &http.Server{
			Addr: url.Host,
		}
		s.Close()
		return tt{
			ctx:   ctx,
			start: srv.ListenAndServe,
			shutdown: func() error {
				return srv.Shutdown(context.Background())
			},
			err: "http: Server closed",
		}
	})
	tests.Add("shutdown failure", func() interface{} {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		return tt{
			ctx:      ctx,
			start:    func() error { return nil },
			shutdown: func() error { return errors.New("bar") },
			err:      "bar",
		}
	})
	tests.Add("serve and shutdown failures", tt{
		ctx: context.Background(),
		start: func() error {
			time.Sleep(10 * time.Millisecond)
			return errors.New("foo")
		},
		shutdown: func() error {
			return errors.New("bar")
		},
		err: "bar",
	})
	tests.Add("non-blocking start success", func() interface{} {
		ctx, cancel := context.WithTimeout(context.Background(), 20*time.Millisecond)
		var mu sync.Mutex
		status := "failure"
		return tt{
			ctx: ctx,
			start: func() error {
				go func() {
					time.Sleep(10 * time.Millisecond)
					mu.Lock()
					status = "success"
					mu.Unlock()
				}()
				return nil
			},
			shutdown: func() error {
				cancel()
				mu.Lock()
				defer mu.Unlock()
				return errors.New(status)
			},
			err: "success",
		}
	})
	tests.Add("empty start and stop", func() interface{} {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		return tt{
			ctx:      ctx,
			start:    func() error { return nil },
			shutdown: func() error { return nil },
		}
	})
	tests.Run(t, func(t *testing.T, tt tt) {
		err := StartServer(tt.ctx, tt.start, tt.shutdown)
		testy.Error(t, tt.err, err)
	})
}

func TestStart(t *testing.T) {
	type tt struct {
		ctx     context.Context
		start   StartFunc
		stop    ShutdownFunc
		timeout time.Duration
		err     string
	}

	tests := testy.NewTable()
	tests.Add("nil start and stop", func() interface{} {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		return tt{
			ctx: ctx,
		}
	})
	tests.Add("startup fails", tt{
		ctx:   context.Background(),
		start: func() error { return errors.New("failed") },
		err:   "failed",
	})
	tests.Add("shutdown failure", func() interface{} {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		return tt{
			ctx:   ctx,
			start: func() error { return nil },
			stop:  func(context.Context) error { return errors.New("bar") },
			err:   "bar",
		}
	})
	tests.Add("success", func() interface{} {
		ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
		defer cancel()
		s := httptest.NewServer(http.HandlerFunc(http.NotFound))
		url, _ := url.Parse(s.URL)
		srv := &http.Server{
			Addr: url.Host,
		}
		s.Close()
		return tt{
			ctx:   ctx,
			start: srv.ListenAndServe,
			stop: func(ctx context.Context) error {
				return srv.Shutdown(ctx)
			},
			timeout: 5 * time.Second,
		}
	})
	tests.Run(t, func(t *testing.T, tt tt) {
		err := Start(tt.ctx, tt.start, tt.stop, tt.timeout)
		testy.Error(t, tt.err, err)
	})
}
