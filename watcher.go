package signal

import (
	"context"
	"os"
	"os/signal"
)

// NewWatcher returns a new context that will cancel when any of sig are
// received.
//
// Deprecated: Use os/signal.NotifyContext in the standard library.
func NewWatcher(ctx context.Context, sig ...os.Signal) (context.Context, context.CancelFunc) {
	return signal.NotifyContext(ctx, sig...)
}
